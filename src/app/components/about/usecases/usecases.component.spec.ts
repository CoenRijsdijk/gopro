import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { UsecasesComponent } from './usecases.component'
import { RouterLinkStubDirective } from 'src/app/app.component.spec'
import { Component, Input } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { Http } from '@angular/http'

@Component({ selector: 'app-usecase', template: '' })
class UseCaseStubComponent {
  @Input() useCase
}

describe('UsecasesComponent', () => {
  let component: UsecasesComponent
  let fixture: ComponentFixture<UsecasesComponent>

  let routerSpy: { navigateByUrl: jasmine.Spy }
  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }

  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])

    TestBed.configureTestingModule({
      declarations: [UsecasesComponent, UseCaseStubComponent],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: Http }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(UsecasesComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', async(() => {
    fixture.detectChanges()
    expect(component).toBeTruthy()
  }))
})
