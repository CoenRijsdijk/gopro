import { Component, OnInit } from '@angular/core'
import { Product } from '../../core/_models/product'
import { ProductService } from 'src/app/core/_services'
import { Category } from 'src/app/core/_models'

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  filter: boolean
  products: Product[]
  arrOfProducts: Product[][]
  categories: Category[]
  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.getProducts()
    this.getCategories()
  }

  getProducts(): void {
    this.productService.getProducts().subscribe(products => {
      console.log(products)
      this.products = products.result
      console.log(this.products)
      this.arrOfProducts = this.getARowOfThreeItems()
    })
    this.filter = false
  }

  getARowOfThreeItems(): Product[][] {
    const result = []
    const chunkSize = 3

    for (var i = 0; i < this.products.length; i += chunkSize) {
      result.push(this.products.slice(i, i + chunkSize))
    }
    return result
  }

  getProductsByCategory(catName: string, catId: string) {
    console.log(catName + catId)
    console.log('getProductsByCat')
    this.productService.getProductsByCat(catId).subscribe(products => {
      console.log(products)
      this.products = products.result
      console.log(this.products)
      this.arrOfProducts = this.getARowOfThreeItems()
    })
    this.filter = true
  }

  getCategories(): void {
    console.log('getCategories')
    this.productService.getCategories().subscribe(categories => {
      console.log(categories)
      this.categories = categories.result
      console.log(this.categories)
    })
  }
}
