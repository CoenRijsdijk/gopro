import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Registreren',
      description: 'Hiermee registreert een nieuwe gebruiker zich.',
      scenario: [
        'De gebruiker vult de benodigde gegevens in',
        'De applicatie valideert de ingevoerde gegevens',
        'Indien de gegevens correct zijn, plaatst de applicatie deze in de database'
      ],
      actor: 'Nieuwe gebruiker',
      precondition: 'Geen',
      postcondition: 'De gebruiker heeft zich geregistreerd'
    },
    {
      id: 'UC-02',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: '-',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-03',
      name: 'Uitloggen',
      description: 'Hiermee logt een bestaande gebruiker uit.',
      scenario: ['Gebruiker klikt op loguit knop.'],
      actor: 'Reguliere gebruiker',
      precondition: 'De gebruiker is ingelogd',
      postcondition: 'De actor is uitgelogd'
    },
    {
      id: 'UC-04',
      name: 'Account wijzigen',
      description: 'Hiermee kan een bestaande gebruiker zijn/haar account wijzigen',
      scenario: [
        'Gebruiker vult nieuwe gegevens in',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn, worden de gewijzigde gegevens opgeslagen'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De gebruiker is ingelogd',
      postcondition: 'Account van gebruiker gewijzigd'
    },
    {
      id: 'UC-05',
      name: 'Account verwijderen',
      description: 'Hiermee kan een bestaande gebruiker zijn/haar account verwijderen',
      scenario: [
        'Gebruiker klikt op account verwijderen',
        'De applicatie verwijdert account van de gebruiker'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De gebruiker is ingelogd',
      postcondition: 'Het account van de gebruiker is verwijderd'
    },
    {
      id: 'UC-06',
      name: 'Producten inzien',
      description: 'Alle gebruikers kunnen bestaande producten inzien',
      scenario: ['Een gebruiker navigeert naar de product pagina', 'De applicatie toont alle producten'],
      actor: 'Alle gebruikers',
      precondition: '-',
      postcondition: 'Een lijst met producten is zichtbaar'
    },
    {
      id: 'UC-07',
      name: 'Producten filteren',
      description: 'Producten filteren op basis van categorie',
      scenario: [
        'Een gebruiker navigeert naar de product pagina',
        'De applicatie toont alle producten',
        'Een gebruiker klikt op een bepaalde categorie',
        'De applicatie toont producten bij bepaalde categorie'
      ],
      actor: 'Alle gebruikers',
      precondition: '-',
      postcondition: 'Een lijst met producten, met een specifieke categorie is zichtbaar'
    },
    {
      id: 'UC-08',
      name: 'Een specifiek product inzien',
      description: 'Alle gebruikers kunnen bestaande producten inzien',
      scenario: [
        'Een gebruiker navigeert naar de product pagina',
        'De applicatie toont alle producten',
        'Gebruiker klikt op details knop van een product',
        'Applicatie toont detail pagina van het geklikte product'
      ],
      actor: 'Alle gebruikers',
      precondition: '-',
      postcondition: 'Een specifiek product is zichtbaar'
    },
    {
      id: 'UC-09',
      name: 'Reviews inzien',
      description: 'Alle gebruikers kunnen reviews bij een specifiek product zien',
      scenario: [
        'Een gebruiker navigeert naar de product pagina',
        'De applicatie toont alle producten',
        'Gebruiker klikt op details knop van een product',
        'Applicatie toont onder de details van het product alle bestaande reviews'
      ],
      actor: 'Alle gebruikers',
      precondition: '-',
      postcondition: 'Reviews bij een specifiek product zijn zichtbaar'
    },
    {
      id: 'UC-10',
      name: 'Review plaatsen',
      description: 'Een reguliere gebruiker kan een review bij een specifiek product plaatsen',
      scenario: [
        'Gebruiker klikt op details knop van een product',
        'Applicatie toont onder de details van het product alle bestaande reviews en een formulier om een nieuw review te plaatsen',
        'Gebruiker vult benodigde gegevens in',
        'Applicatie valideert deze gegevens en plaatst een review'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'Een review bij een specifiek product is geplaatst'
    },
    {
      id: 'UC-11',
      name: 'Product toevoegen aan winkelwagen',
      description: 'Een gebruiker kan een product toevoegen aan de winkelwagen',
      scenario: [
        'Gebruiker klikt op knop "voeg toe" van een product',
        'Applicatie plaatst product in de winkelwagen'
      ],
      actor: 'Alle gebruikers',
      precondition: '-',
      postcondition: 'Een product is in de winkelwagen geplaatst'
    },
    {
      id: 'UC-12',
      name: 'Producten zien in de winkelwagen',
      description: 'Een gebruiker kan alle toegevoegde producten zien in de winkelwagen',
      scenario: [
        'Gebruiker klikt in de navigatiebalk op winkelwagen',
        'Applicatie navigeert naar de winkelwagen'
      ],
      actor: 'Alle gebruikers',
      precondition: '-',
      postcondition: 'Winkelwagen met toegevoegde producten is zichtbaar'
    },
    {
      id: 'UC-13',
      name: 'Producten verwijderen uit winkelwagen',
      description: 'Een reguliere gebruiker kan een reeds toegevoegd product uit de winkelwagen verwijderen',
      scenario: [
        'Gebruiker klikt op prullenbakje, bij een product, in de winkelwagen',
        'Applicatie verwijdert product uit winkelwagen'
      ],
      actor: 'Alle gebruikers',
      precondition: 'Winkelwagen moet product(en) bevatten',
      postcondition: 'Product uit winkelwagen is verwijderd'
    },
    {
      id: 'UC-14',
      name: 'Order met producten plaatsen',
      description: 'Een reguliere gebruiker kan een order met producten plaatsten',
      scenario: ['Gebruiker klikt op "bestellen" button in winkelwagen', 'Applicatie voegt nieuwe order toe'],
      actor: 'Reguliere gebruiker',
      precondition: 'Winkelwagen moet producten bevatten',
      postcondition: 'Order met bijbehorende producten is geplaatst'
    },
    {
      id: 'UC-15',
      name: 'Alle geplaatste orders zien',
      description: 'Een reguliere gebruiker kan alle geplaatste orders inzien met producten',
      scenario: [
        'Gebruiker klikt op poppetje + gebruikersnaam in de navigatiebalk',
        'Applicatie navigeert naar order overview pagina',
        'Applicatie laat orders zien, als die geplaatst zijn'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Winkelwagen moet producten bevatten/gebruiker moet ingelogd zijn',
      postcondition: 'Order met bijbehorende producten wordt getoond'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
