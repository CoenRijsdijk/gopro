import { Injectable } from '@angular/core'
import { Product } from '../_models/product'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { environment } from 'src/environments/environment'
import { tap, catchError, map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) {}

  getProducts(): Observable<any> {
    console.log('getProducts')
    console.log(`${environment.apiUrl}/api/products`)
    return this.http.get<any>(`${environment.apiUrl}/api/products`).pipe(
      tap(_ => console.log('fetched products')),
      catchError(this.handleError<any>('getProducts', []))
    )
  }

  getProductById(id: string): Observable<Product> {
    console.log('getProductsById')
    return this.http
      .get<any>(`${environment.apiUrl}/api/products/${id}`)
      .pipe(catchError(this.handleError<any>('getProduct')))
  }

  getCategories(): Observable<any> {
    console.log('getCategories')
    return this.http.get<any>(`${environment.apiUrl}/api/category`).pipe(
      tap(_ => console.log(_ + 'fetched categories')),
      catchError(this.handleError<any>('getCategories', []))
    )
  }

  getProductsByCat(id: string): Observable<any> {
    console.log('getProductsByCat')
    return this.http.get<any>(`${environment.apiUrl}/api/products/category/${id}`).pipe(
      tap(_ => console.log(_ + 'fetched products with category id ' + id)),
      catchError(this.handleError<any>('getProductsByCat', []))
    )
  }

  addReview(prodId: string, title: string, content: string, rating: number): Observable<any> {
    const token = localStorage.getItem('token')
    const headers = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': `${token}` })
    }
    console.log(token)
    console.log('addReview')
    console.log(`${environment.apiUrl}/api/product/${prodId}/review`, { headers: headers })
    const data = {
      title: title,
      content: content,
      rating: rating
    }
    console.log(data)
    return this.http.post(`${environment.apiUrl}/api/product/${prodId}/review`, data, headers).pipe(
      tap(response => console.log(response)),
      catchError(this.handleError<any>('add review'))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
