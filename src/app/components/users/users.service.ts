import { Injectable } from '@angular/core'
import { Observable, BehaviorSubject, throwError, of } from 'rxjs'
import { map, tap, retry, catchError } from 'rxjs/operators'
import { User } from './user.model'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment'
import { AuthenticationService } from 'src/app/core/_services'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly currentUser = 'currentuser'
  user: User

  usersAvailable = new BehaviorSubject<boolean>(false)

  constructor(private http: HttpClient, private authService: AuthenticationService) {
    console.log('UserService constructed')
    console.log(`Connected to ${environment.apiUrl}`)
  }

  updateUser(user: User) {
    const token = localStorage.getItem('token')
    const headers = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': `${token}` })
    }
    console.log('updateUser' + token + 'USER ID' + `${user._id}`)
    console.log(`${environment.apiUrl}/api/users/me/${user._id}`, user, headers)
    return this.http
      .put(`${environment.apiUrl}/api/users/me/${user._id}`, user, headers)
      .pipe(
        tap(response => console.log(response)),
        catchError(this.handleError<any>('update user'))
      )
      .subscribe({
        next: response => {
          const currentUser = new User(response)
          console.dir(currentUser)
          this.saveCurrentUser(currentUser, response.token, 'true')

          // Notify all listeners that we're logged in.
          this.authService.isLoggedInUser.next(true)
          this.authService.loggedInUserName.next(currentUser.fullName)
          console.dir(currentUser)
        },
        error: err => {
          console.error('Error logging in: ' + err)
        }
      })
  }

  deleteUser(user: User): Observable<any> {
    const token = localStorage.getItem('token')
    const headers = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': `${token}` })
    }
    console.log('delete')
    console.log(`${environment.apiUrl}/api/users/me/${user._id}`)
    return this.http.delete(`${environment.apiUrl}/api/users/me/${user._id}`, headers).pipe(
      tap(response => console.log(response)),
      catchError(this.handleError('delete user'))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }

  private saveCurrentUser(user: User, token: string, isLoggedIn: string): void {
    localStorage.setItem(this.authService.isAlreadyLoggedIn, isLoggedIn)
    localStorage.setItem(this.authService.currentUser, JSON.stringify(user))
    localStorage.setItem(this.authService.currentToken, token)
  }

  read() {
    //const jwt = this.getToken()
    //let jwtData = jwt.split('.')[1]
    //let decodedJwtJsonData = window.atob(jwtData)
    //let jsonObject = JSON.parse(decodedJwtJsonData)
    //console.log(jsonObject)
    //console.log(jsonObject.sub.email + jsonObject.sub.id)
    //const email = jsonObject.sub.email
    //const id = jsonObject.sub.id

    //return of(jsonObject)
    console.log(JSON.parse(localStorage.getItem(this.currentUser)))
    return of(JSON.parse(localStorage.getItem(this.currentUser)))
  }

  getToken() {
    return localStorage.getItem('token')
  }
}

/**
 * This interface specifies the structure of the expected API server response.
 */
export interface ApiResponse {
  results: any[]
}
