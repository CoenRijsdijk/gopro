import { Component, OnInit } from '@angular/core'
import { FormGroup, Validators, FormControl } from '@angular/forms'
import { first } from 'rxjs/operators'

import { AuthenticationService } from '../../../core/_services'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup

  constructor(private authService: AuthenticationService) {}

  ngOnInit() {
    this.registerForm = new FormGroup({
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, this.validEmail.bind(this)]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
        this.validPassword.bind(this)
      ]),
      birthDay: new FormControl(null, [Validators.required]),
      street: new FormControl(null, [Validators.required]),
      houseNumber: new FormControl(null, [Validators.required]),
      postalCode: new FormControl(null, [
        Validators.required,
        Validators.maxLength(7),
        this.validPostalCode.bind(this)
      ]),
      city: new FormControl(null, [Validators.required]),
      country: new FormControl(null, [Validators.required])
    })
  }
  onSubmit() {
    if (this.registerForm.valid) {
      const firstName = this.registerForm.value['firstName']
      const lastName = this.registerForm.value['lastName']
      const email = this.registerForm.value['email']
      const password = this.registerForm.value['password']
      const birthDay = this.registerForm.value['birthDay']
      const street = this.registerForm.value['street']
      const houseNumber = this.registerForm.value['houseNumber']
      const postalCode = this.registerForm.value['postalCode']
      const city = this.registerForm.value['city']
      const country = this.registerForm.value['country']
      this.authService.register(
        firstName,
        lastName,
        email,
        password,
        birthDay,
        street,
        houseNumber,
        postalCode,
        city,
        country
      )
    } else {
      console.log('registerForm invalid')
    }
  }
  validEmail(control: FormControl): { [s: string]: boolean } {
    const email = control.value
    const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    if (regexp.test(email) !== true) {
      return { email: false }
    } else {
      return null
    }
  }

  validPassword(control: FormControl): { [s: string]: boolean } {
    const password = control.value
    const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
    const test = regexp.test(password)
    if (regexp.test(password) !== true) {
      return { password: false }
    } else {
      return null
    }
  }

  validPostalCode(control: FormControl): { [s: string]: boolean } {
    const postalCode = control.value
    const regexp = new RegExp('[1-9][0-9]{3}\\s?[a-zA-Z]{2}')
    const test = regexp.test(postalCode)
    console.log(test)
    if (regexp.test(postalCode) !== true) {
      return { postalCode: false }
    } else {
      return null
    }
  }
}
