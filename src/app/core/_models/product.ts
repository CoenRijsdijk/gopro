import { Category } from './category'

export class Product {
  id: string
  name: string
  description: string
  price: number
  imageUrl: string
  rating: number
  reviews: []
  category: Category
}
