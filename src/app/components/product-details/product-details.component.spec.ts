import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ProductDetailsComponent } from './product-details.component'
import { RouterLinkStubDirective } from 'src/app/app.component.spec'
import { Component, Input } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { Http } from '@angular/http'
import { HttpClient } from '@angular/common/http'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RatingModule } from 'ng-starrating'
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap'
import { ProductService } from 'src/app/core/_services/product.service'
import { of } from 'rxjs'
import { Product } from 'src/app/core/_models/product'

@Component({ selector: 'star-rating', template: '' })
class RatingStubComponent {
  @Input() value
}

describe('ProductDetailsComponent', () => {
  let component: ProductDetailsComponent
  let fixture: ComponentFixture<ProductDetailsComponent>

  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }
  let productServiceSpy: {
    getProducts: jasmine.Spy
    getProductById: jasmine.Spy
    getReviewsByProduct: jasmine.Spy
    getCategories: jasmine.Spy
    getProductsByCat: jasmine.Spy
    addReview: jasmine.Spy
  }

  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    productServiceSpy = jasmine.createSpyObj('ProductService', [
      'getProducts',
      'getProductById',
      'getCategories',
      'getProductsByCat',
      'addReview'
    ])

    TestBed.configureTestingModule({
      declarations: [ProductDetailsComponent, RatingStubComponent],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, NgbRatingModule],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: ProductService, useValue: productServiceSpy },
        { provide: HttpClient }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(ProductDetailsComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', async(() => {
    productServiceSpy.getProductById.and.returnValue(of(new Product()))
    fixture.detectChanges()
    expect(component).toBeTruthy()
  }))
})
