import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { UserProfileComponent } from './user-profile.component'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { HttpClient } from '@angular/common/http'
import { Http } from '@angular/http'

describe('UserProfileComponent', () => {
  let component: UserProfileComponent
  let fixture: ComponentFixture<UserProfileComponent>

  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }
  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    TestBed.configureTestingModule({
      declarations: [UserProfileComponent],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, NgbRatingModule],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: HttpClient },
        { provide: Http }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
