export class User {
  id: number
  email: string
  password: string
  firstName: string
  lastName: string
  birthDay: Date
  street: string
  houseNumber: number
  postalCode: string
  city: string
  country: string
  orders: []

  constructor(values: Object = {}) {
    Object.assign(this, values)
  }
  get fullName() {
    return this.firstName + ' ' + this.lastName
  }
}
