import { Component, OnInit } from '@angular/core'
import { OrderService } from 'src/app/core/_services/order.service'
import { Order } from 'src/app/core/_models'
import { Item } from 'src/app/core/_models/item'

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  total: number = 0
  orders: Order[]

  constructor(private orderService: OrderService) {}

  ngOnInit() {
    this.getOrders()
  }

  getOrders(): void {
    this.orderService.getOrders().subscribe(orders => {
      console.log(orders)
      this.orders = orders.result.orders
    })
  }
}
