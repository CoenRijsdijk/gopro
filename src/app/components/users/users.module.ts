import { NgModule } from '@angular/core'
// import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common'
import { UserEditComponent } from './user-edit/user-edit.component'
import { UserDeleteComponent } from './user-delete/user-delete.component'
import { UserRoutingModule } from './users-routing.module'
import { UsersComponent } from './users.component'
import { UserNotfoundComponent } from './user-notfound/user-notfound.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientModule } from '@angular/common/http'
import { UserRoleNamePipe } from './user.model'
import { UserProfileComponent } from './user-profile/user-profile.component'

@NgModule({
  declarations: [
    UsersComponent,
    UserEditComponent,
    UserProfileComponent,
    UserDeleteComponent,
    UserNotfoundComponent,
    UserRoleNamePipe
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, HttpClientModule, NgbModule, UserRoutingModule],
  providers: [],
  exports: [UsersComponent]
})
export class UsersModule {}
