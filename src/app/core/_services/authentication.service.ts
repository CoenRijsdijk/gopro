import { Injectable } from '@angular/core'
import { map, tap } from 'rxjs/operators'
import { Http, Headers } from '@angular/http'
import { BehaviorSubject, Observable } from 'rxjs'
import { AlertService } from '../../modules/alert/alert.service'
import { Router } from '@angular/router'
import { User } from '../../components/users/user.model'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  isLoggedInUser = new BehaviorSubject<boolean>(false)
  // Username for printing in navbar
  loggedInUserName = new BehaviorSubject<string>('')

  readonly currentUser = 'currentuser'
  readonly currentToken = 'token'
  readonly isAlreadyLoggedIn = 'isloggedin'

  // store the URL so we can redirect after logging in
  public readonly redirectUrl: string = '/dashboard'

  private readonly headers = new Headers({ 'Content-Type': 'application/json' })

  /**
   *
   * @param alertService
   * @param router
   * @param http
   */
  constructor(private alertService: AlertService, private router: Router, private http: Http) {
    this.getCurrentUser().subscribe({
      next: (user: User) => {
        console.log(`${user.email} logged in`)
        this.isLoggedInUser.next(true)
        this.loggedInUserName.next(user.fullName)
      },
      error: message => {
        this.isLoggedInUser.next(false)
        // this.router.navigate(['/login']);
      }
    })
  }

  register(
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    birthDay: Date,
    street: string,
    houseNumber: string,
    postalCode: string,
    city: string,
    country: string
  ) {
    console.log('register')

    console.log(`${environment.apiUrl}/api/register`)
    const name = {
      firstname: firstName,
      lastname: lastName
    }
    console.log(firstName + lastName + email + password + name.firstname + name.lastname)
    return (
      this.http
        .post(
          `${environment.apiUrl}/api/register`,
          {
            firstname: firstName,
            lastname: lastName,
            email: email,
            password: password,
            name,
            birthday: birthDay,
            street: street,
            housenumber: houseNumber,
            postalcode: postalCode,
            city: city,
            country: country
          },
          { headers: this.headers }
        )
        .pipe(
          map(response => response.json()),
          tap(console.log)
        )
        /*.subscribe(
        res => console.log(res),
        err => console.log(err)
      )*/
        .subscribe({
          next: response => {
            const currentUser = new User(response)
            console.dir(currentUser)
            this.saveCurrentUser(currentUser, response.token, 'true')

            // Notify all listeners that we're logged in.
            this.isLoggedInUser.next(true)
            this.loggedInUserName.next(currentUser.fullName)
            //currentUser.hasRole(UserRole.Admin).subscribe(result => this.isAdminUser.next(result))
            //currentUser.hasRole(UserRole.Basic).subscribe(result => this.isPlainUser.next(result))

            this.alertService.success('Succesvol geregistreerd!')
            //
            // If redirectUrl exists, go there
            this.router.navigate([this.redirectUrl])
            return true
          },
          error: err => {
            console.error('Error logging in: ' + err)
            this.alertService.error('Invalide gegevens!')
          }
        })
    )
  }

  login(email: string, password: string) {
    console.log('login')
    console.log(`${environment.apiUrl}/api/login`)
    return this.http
      .post(
        `${environment.apiUrl}/api/login`,
        { email: email, password: password },
        { headers: this.headers }
      )
      .pipe(
        map(response => response.json()),
        tap(console.log)
      )
      .subscribe({
        next: response => {
          console.log(response)
          const currentUser = new User(response)
          console.dir(currentUser)
          this.saveCurrentUser(currentUser, response.token, 'true')

          // Notify all listeners that we're logged in.
          this.isLoggedInUser.next(true)
          this.loggedInUserName.next(currentUser.fullName)

          this.alertService.success('U bent ingelogd!')
          // If redirectUrl exists, go there
          this.router.navigate([this.redirectUrl])
          return true
        },
        error: err => {
          // console.error('Error logging in: ' + err);
          this.alertService.error('Invalide gegevens!')
        }
      })
  }

  /**
   * Log out.
   */
  logout() {
    console.log('logout')
    localStorage.removeItem(this.currentUser)
    localStorage.removeItem(this.currentToken)
    localStorage.removeItem(this.isAlreadyLoggedIn)
    this.isLoggedInUser.next(false)
    //this.isAdminUser.next(false)
    this.alertService.success('U bent uitgelogd')
  }

  /**
   * Get the currently logged in user.
   */
  private getCurrentUser(): Observable<User> {
    return new Observable(observer => {
      const localUser: any = JSON.parse(localStorage.getItem(this.currentUser))
      console.log('localUser', localUser)
      if (localUser) {
        console.log('localUser found')
        observer.next(new User(localUser))
        observer.complete()
      } else {
        console.log('NO localUser found')
        observer.error('NO localUser found')
        observer.complete()
      }
    })
  }

  /**
   *
   */
  private saveCurrentUser(user: User, token: string, isLoggedIn: string): void {
    localStorage.setItem(this.isAlreadyLoggedIn, isLoggedIn)
    localStorage.setItem(this.currentUser, JSON.stringify(user))
    localStorage.setItem(this.currentToken, token)
  }

  get userFullName(): Observable<string> {
    console.log('userFullName ' + this.loggedInUserName.value)
    return this.loggedInUserName.asObservable()
  }

  /**
   *
   */
  get userIsLoggedIn(): Observable<boolean> {
    console.log('userIsLoggedIn() ' + this.isLoggedInUser.value)
    return this.isLoggedInUser.asObservable()
  }
}
