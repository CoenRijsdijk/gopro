import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Product } from 'src/app/core/_models/product'
import { ProductService } from 'src/app/core/_services'
import { OrderService } from 'src/app/core/_services/order.service'
import { Item } from 'src/app/core/_models/item'
import { AlertService } from 'src/app/modules/alert/alert.service'

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  product: Product
  items: Item[] = []
  total: number = 0
  products: Item[] = []

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private orderService: OrderService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    if (!localStorage.getItem('cart')) {
      localStorage.setItem('cart', JSON.stringify([]))
    }

    this.activatedRoute.params.subscribe(params => {
      const id = params['id']
      if (id) {
        this.fetchProduct()
      }
    })
    this.loadCart()
  }

  addOrder() {
    const orderStatus = 'Active'
    let cart = JSON.parse(localStorage.getItem('cart'))
    console.log(cart)
    if (cart.length === 0 || !cart || cart.length < 0 || cart === []) {
      this.alertService.error('Voeg eerst producten toe, voordat een bestelling kan plaatsvinden!')
    } else {
      for (let i = 0; i < cart.length; i++) {
        let item = JSON.parse(cart[i])
        console.log(item.product.id)
        console.log(item)
        this.products.push(item)
      }
      console.log(this.products)
      this.orderService.addOrder(orderStatus, this.products).subscribe()
      this.alertService.success('Bestelling succesvol toegevoegd!')
      localStorage.removeItem('cart')
      localStorage.setItem('cart', JSON.stringify([]))
      this.loadCart()
    }
  }

  addProductToCart() {
    this.activatedRoute.params.subscribe(params => {
      const id = params['id']
      if (id) {
        console.log(this.product)
        const item: Item = {
          product: this.product,
          quantity: 1
        }
        console.log(id)
        console.log(item.product.id)
        console.log(item.product.id == id)
        if (localStorage.getItem('cart') == null) {
          let cart: any = []
          cart.push(JSON.stringify(item))
          localStorage.setItem('cart', JSON.stringify(cart))
        } else {
          let cart: any = JSON.parse(localStorage.getItem('cart'))
          let index: number = -1
          for (let i = 0; i < cart.length; i++) {
            let item: Item = JSON.parse(cart[i])
            if (item.product.id == id) {
              index = i
              break
            }
          }
          if (index == -1) {
            cart.push(JSON.stringify(item))
            localStorage.setItem('cart', JSON.stringify(cart))
          } else {
            let item: Item = JSON.parse(cart[index])
            item.quantity += 1
            cart[index] = JSON.stringify(item)
            localStorage.setItem('cart', JSON.stringify(cart))
          }
        }
        this.loadCart()
      } else {
        this.loadCart()
      }
    })
  }

  loadCart(): void {
    this.total = 0
    this.items = []
    let cart = JSON.parse(localStorage.getItem('cart'))
    for (let i = 0; i < cart.length; i++) {
      let item = JSON.parse(cart[i])
      this.items.push({
        product: item.product,
        quantity: item.quantity
      })
      this.total += item.product.price * item.quantity
    }
  }

  remove(id: string): void {
    let cart: any = JSON.parse(localStorage.getItem('cart'))
    let index: number = -1
    for (let i = 0; i < cart.length; i++) {
      let item: Item = JSON.parse(cart[i])
      if (item.product.id == id) {
        cart.splice(i, 1)
        break
      }
    }
    localStorage.setItem('cart', JSON.stringify(cart))
    this.loadCart()
  }

  fetchProduct(): void {
    console.log('fetched prod')
    const id = this.activatedRoute.snapshot.paramMap.get('id')
    this.productService.getProductById(id).subscribe(
      product => {
        console.log(product)
        this.product = product
        //this.product = new Product()
        /*this.product.id = product.id
      this.product.name = product.name
      this.product.description = product.description
      this.product.imageUrl = product.imageUrl
      this.product.price = product.price
      this.product.rating = product.rating*/

        console.log(product.name)
        console.log(this.product.id)
        console.log(this.product)
        //Hier heb je pas toegang tot je product, dus:
        this.addProductToCart()
      },
      error => console.log('Error ' + error)
    )
  }
}
