import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { UsersModule } from './components/users/users.module'
import { LoginComponent } from './components/auth/login/login.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { AlertModule } from './modules/alert/alert.module'
import { AlertService } from './modules/alert/alert.service'
import { RatingModule } from 'ng-starrating'
import { RegisterComponent } from './components/auth/register/register.component'

import { UsecasesComponent } from './components/about/usecases/usecases.component'
import { NavbarComponent } from './components/navbar/navbar.component'
import { UsecaseComponent } from './components/about/usecases/usecase/usecase.component'
import { ProductListComponent } from './components/product-list/product-list.component'
import { ProductDetailsComponent } from './components/product-details/product-details.component'
import { CartComponent } from './components/cart/cart.component'
import { LoggedInAuthGuard } from './core/_guards/auth.guards'
import { ProductService } from './core/_services'
import { OrderService } from './core/_services/order.service'
import { OrderComponent } from './components/order/order.component'

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    ProductListComponent,
    ProductDetailsComponent,
    CartComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AlertModule,
    // UsersModule must be before AppRoutingModule,
    // otherwise userroutes are overwritten by '**'.
    UsersModule,
    AppRoutingModule,
    NgbModule,
    RatingModule
  ],
  providers: [
    LoggedInAuthGuard,
    // AdminRoleAuthGuard,
    AlertService,
    OrderService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
