import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { RegisterComponent } from './register.component'
import { RouterLinkStubDirective } from 'src/app/app.component.spec'
import { Component } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { Http } from '@angular/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

describe('RegisterComponent', () => {
  let component: RegisterComponent
  let fixture: ComponentFixture<RegisterComponent>

  let routerSpy: { navigateByUrl: jasmine.Spy }
  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }

  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])

    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: Http }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(RegisterComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('registerForm invalid when empty', async(() => {
    component.ngOnInit()
    component.registerForm.controls['firstName'].setValue('')
    component.registerForm.controls['lastName'].setValue('')
    component.registerForm.controls['email'].setValue('')
    component.registerForm.controls['password'].setValue('')
    component.registerForm.controls['birthDay'].setValue('')
    component.registerForm.controls['street'].setValue('')
    component.registerForm.controls['houseNumber'].setValue('')
    component.registerForm.controls['postalCode'].setValue('')
    component.registerForm.controls['city'].setValue('')
    component.registerForm.controls['country'].setValue('')
    expect(component.registerForm.valid).toBeFalsy()
  }))

  it('registerForm invalid when empty', async(() => {
    component.ngOnInit()
    component.registerForm.controls['firstName'].setValue('Coen')
    component.registerForm.controls['lastName'].setValue('Rijsdijk')
    component.registerForm.controls['email'].setValue('coenrijsdijk@gmail.com')
    component.registerForm.controls['password'].setValue('Coen12345')
    component.registerForm.controls['birthDay'].setValue('19-04-2000')
    component.registerForm.controls['street'].setValue('Lagendijk')
    component.registerForm.controls['houseNumber'].setValue('132')
    component.registerForm.controls['postalCode'].setValue('2981EN')
    component.registerForm.controls['city'].setValue('Ridderkerk')
    component.registerForm.controls['country'].setValue('Nederland')
    expect(component.registerForm.valid).toBeTruthy()
  }))

  it('should create', async(() => {
    fixture.detectChanges()
    expect(component).toBeTruthy()
  }))
})
