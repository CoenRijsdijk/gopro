import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ProductListComponent } from './product-list.component'
import { RouterLinkStubDirective } from 'src/app/app.component.spec'
import { Component } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { Http } from '@angular/http'
import { RouterTestingModule } from '@angular/router/testing'
import { RatingModule } from 'ng-starrating'
import { HttpClient } from '@angular/common/http'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { ProductService } from 'src/app/core/_services/product.service'
import { of } from 'rxjs'
import { Product } from 'src/app/core/_models'

describe('ProductListComponent', () => {
  let component: ProductListComponent
  let fixture: ComponentFixture<ProductListComponent>

  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }
  let productServiceSpy: {
    getProducts: jasmine.Spy
    getProductById: jasmine.Spy
    getReviewsByProduct: jasmine.Spy
    getCategories: jasmine.Spy
    getProductsByCat: jasmine.Spy
    addReview: jasmine.Spy
  }

  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    productServiceSpy = jasmine.createSpyObj('ProductService', [
      'getProducts',
      'getProductById',
      'getCategories',
      'getProductsByCat',
      'addReview'
    ])

    TestBed.configureTestingModule({
      declarations: [ProductListComponent],
      imports: [RouterTestingModule, RatingModule, NgbModule],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: ProductService, useValue: productServiceSpy },
        { provide: HttpClient }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(ProductListComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', async(() => {
    productServiceSpy.getProducts.and.returnValue(of())
    productServiceSpy.getCategories.and.returnValue(of())
    fixture.detectChanges()
    expect(component).toBeTruthy()
  }))
})
