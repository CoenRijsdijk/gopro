import { Component, OnInit, Input } from '@angular/core'
import { formatDate } from '@angular/common'
import { ActivatedRoute, Router, Params } from '@angular/router'
import { UserService } from '../users.service'
import { AuthenticationService } from '../../../core/_services/authentication.service'
import { Subscription, Observable } from 'rxjs'
import { User } from '../user.model'

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.scss']
})
export class UserDeleteComponent implements OnInit {
  id: number
  user: User
  isLoggedIn$: Observable<boolean>
  isAdmin$: Observable<boolean>
  isActive: boolean
  submitted = false

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.isLoggedIn$ = this.authService.userIsLoggedIn
    // Get my user information - get from server, based on JWT token
    this.userService.read().subscribe(user => (this.user = new User(user)))
    console.log(this.user)
    console.log(this.user._id)
    //this.id = this.route.snapshot.paramMap.get('id')
    //console.log(this.id)
    //this.userService.getUser(this.id)
    //this.title = this.route.snapshot.data['title'] || 'Delete User'
    console.log(this.user)
    /*this.title = this.route.snapshot.data['title'] || 'User Details'
        this.isLoggedIn$ = this.authService.userIsLoggedIn
        // Get my user information - get from server, based on JWT token
        this.userService.read().subscribe(user => (this.user = new User(user)))
        console.log(this.user)
        console.log(this.user._id)*/
  }

  onSubmit() {
    this.submitted = true
    console.log('verwijderen')

    this.userService.deleteUser(this.user).subscribe()
    localStorage.removeItem(this.authService.currentUser)
    localStorage.removeItem(this.authService.currentToken)
    localStorage.removeItem(this.authService.isAlreadyLoggedIn)
    this.authService.isLoggedInUser.next(false)
    this.router.navigate(['/dashboard'], { relativeTo: this.route })
  }
}
