import { Component, OnInit } from '@angular/core'
import { ProductService } from 'src/app/core/_services'
import { ActivatedRoute } from '@angular/router'
import { Product, Review } from 'src/app/core/_models'
import { User } from '../users/user.model'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap'
import { AlertService } from 'src/app/modules/alert/alert.service'

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  reviewForm: FormGroup
  product: Product
  reviews: Review[]
  user: User
  constructor(
    private alertService: AlertService,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    config: NgbRatingConfig
  ) {
    config.max = 5
  }

  ngOnInit() {
    this.reviewForm = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      content: new FormControl(null, [Validators.required]),
      rating: new FormControl(null, [Validators.required, this.validRating.bind(this)])
    })
    this.getProduct()
    //this.getUserById()
  }

  onSubmit() {
    console.log(
      this.reviewForm.value['rating'] + this.reviewForm.value['title'] + this.reviewForm.value['content']
    )
    const prodId = this.activatedRoute.snapshot.paramMap.get('id')
    if (this.reviewForm.valid) {
      const title = this.reviewForm.value['title']
      const content = this.reviewForm.value['content']
      const rating = this.reviewForm.value['rating']
      this.productService.addReview(prodId, title, content, rating).subscribe()
      this.alertService.success('Review succesvol toegevoegd!')
    } else {
      this.alertService.error('Review is niet toegevoegd!')
      console.error('reviewForm invalid')
    }
  }

  getProduct(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id')
    this.productService.getProductById(id).subscribe(p => {
      console.log(p)
      this.product = p
      this.reviews = p.reviews
    })
  }
  validRating(control: FormControl): { [s: string]: boolean } {
    const rating = control.value
    if (!rating || rating < 0 || rating > 5) {
      return { rating: false }
    } else {
      return null
    }
  }
}
