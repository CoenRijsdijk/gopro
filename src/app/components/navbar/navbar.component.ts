import { Component, Input, OnInit, OnDestroy } from '@angular/core'

import { User } from '../../core/_models'
import { UserService, AuthenticationService } from '../../core/_services'
import { Observable, Subscription } from 'rxjs'
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input() apptitle: string
  isLoggedIn$: Observable<boolean>
  isAdmin$: Observable<boolean>
  fullNameSubscription: Subscription
  isNavbarCollapsed = true
  fullName: string = ''

  constructor(private authService: AuthenticationService, private router: Router) {}

  ngOnInit() {
    this.isLoggedIn$ = this.authService.userIsLoggedIn
    //this.isAdmin$ = this.authService.userIsAdmin;
    this.fullNameSubscription = this.authService.userFullName.subscribe(name => (this.fullName = name))
  }

  onLogout() {
    this.authService.logout()
  }

  ngOnDestroy() {
    this.fullNameSubscription.unsubscribe()
  }
}
