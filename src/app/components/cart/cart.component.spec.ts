import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CartComponent } from './cart.component'
import { RouterLinkStubDirective } from 'src/app/app.component.spec'
import { Component } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { Http } from '@angular/http'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClient } from '@angular/common/http'
import { ProductService } from 'src/app/core/_services/product.service'

describe('CartComponent', () => {
  let component: CartComponent
  let fixture: ComponentFixture<CartComponent>
  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }

  beforeEach(async(() => {
    let store = {}
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`
      },
      removeItem: (key: string) => {
        delete store[key]
      },
      clear: () => {
        store = {}
      }
    }

    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem)
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem)
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem)
    spyOn(localStorage, 'clear').and.callFake(mockLocalStorage.clear)

    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])

    TestBed.configureTestingModule({
      declarations: [CartComponent],
      imports: [RouterTestingModule],
      providers: [{ provide: AlertService, useValue: alertServiceSpy }, { provide: HttpClient }]
    }).compileComponents()

    fixture = TestBed.createComponent(CartComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', async(() => {
    fixture.detectChanges()
    expect(component).toBeTruthy()
  }))
})
