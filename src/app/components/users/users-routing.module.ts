import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UserDeleteComponent } from './user-delete/user-delete.component'
import { UserEditComponent } from './user-edit/user-edit.component'
import { UserProfileComponent } from './user-profile/user-profile.component'

const routes: Routes = [
  { path: 'users/me', component: UserProfileComponent },
  { path: 'users/me/edit/:id', component: UserEditComponent },
  { path: 'users/me/delete/:id', component: UserDeleteComponent }
]

@NgModule({
  imports: [
    // Always use forChild in child route modules!
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UserRoutingModule {}
