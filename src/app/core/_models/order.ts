import { User } from 'src/app/components/users/user.model'
import { Item } from './item'

export class Order {
  id: number
  orderStatus: string
  orderDate: Date
  products: Item[]
  user: User
}
