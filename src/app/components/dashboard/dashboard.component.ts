import { Component, OnInit } from '@angular/core'
import { Observable, Subscription, BehaviorSubject } from 'rxjs'
import { AuthenticationService } from 'src/app/core/_services'
import { AlertService } from 'src/app/modules/alert/alert.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isLoggedIn$: Observable<boolean>
  private isLoggedIn = new BehaviorSubject<boolean>(localStorage.getItem('isloggedin') === 'true')
  isAdmin$: Observable<boolean>
  fullNameSubscription: Subscription
  fullName: string = ''

  constructor(private authService: AuthenticationService) {}

  ngOnInit() {
    console.log(this.isLoggedIn.value)
    /*if (this.isLoggedIn.value) {
      console.log(this.isLoggedIn.value)
      this.fullNameSubscription = this.authService.userFullName.subscribe(name => (this.fullName = name))
    }*/
    this.isLoggedIn$ = this.authService.userIsLoggedIn
    //this.isAdmin$ = this.authService.userIsAdmin;
    this.fullNameSubscription = this.authService.userFullName.subscribe(name => (this.fullName = name))
    console.log(this.fullName + this.isLoggedIn$)
  }
}
