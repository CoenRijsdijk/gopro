import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { UserEditComponent } from './user-edit.component'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { ProductService } from 'src/app/core/_services'
import { HttpClient } from '@angular/common/http'
import { Http } from '@angular/http'

describe('UserEditComponent', () => {
  let component: UserEditComponent
  let fixture: ComponentFixture<UserEditComponent>

  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }
  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    TestBed.configureTestingModule({
      declarations: [UserEditComponent],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, NgbRatingModule],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: HttpClient },
        { provide: Http }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
