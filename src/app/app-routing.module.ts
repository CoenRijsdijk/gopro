import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { UsecasesComponent } from './components/about/usecases/usecases.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { RegisterComponent } from './components/auth/register/register.component'
import { LoginComponent } from './components/auth/login/login.component'
import { ProductListComponent } from './components/product-list/product-list.component'
import { ProductDetailsComponent } from './components/product-details/product-details.component'
import { CartComponent } from './components/cart/cart.component'
import { OrderComponent } from './components/order/order.component'

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'about', component: UsecasesComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'products', component: ProductListComponent },
  { path: 'products/:id', component: ProductDetailsComponent },
  { path: 'cart', component: CartComponent },
  { path: 'order', component: OrderComponent },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
