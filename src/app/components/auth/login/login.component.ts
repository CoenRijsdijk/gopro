import { Component, OnInit, OnDestroy } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { AuthenticationService } from '../../../core/_services/authentication.service'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loading: boolean = false
  loginForm: FormGroup
  subs: Subscription

  constructor(private authService: AuthenticationService, private router: Router) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        this.validEmail.bind(this)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.pattern('^[a-zA-Z]([a-zA-Z0-9]){2,14}'),
        this.validPassword.bind(this)
      ])
    })

    this.subs = this.authService.userIsLoggedIn.subscribe(alreadyLoggedIn => {
      if (alreadyLoggedIn) {
        console.log('User already logged in > to dashboard')
        this.router.navigate(['/dashboard'])
      }
    })
  }

  ngOnDestroy() {
    this.subs.unsubscribe()
  }

  onSubmit() {
    console.log(this.loading)
    if (this.loginForm.valid) {
      this.canLoading()
      console.log(this.loading)
      const email = this.loginForm.value['email']
      const password = this.loginForm.value['password']
      this.authService.login(email, password)
    } else {
      this.canLoading()
      console.log(this.loading)
      console.error('loginForm invalid')
    }
    this.loading = false
  }

  validEmail(control: FormControl): { [s: string]: boolean } {
    const email = control.value
    const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    if (regexp.test(email) !== true) {
      return { email: false }
    } else {
      return null
    }
  }

  validPassword(control: FormControl): { [s: string]: boolean } {
    const password = control.value
    const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
    const test = regexp.test(password)
    if (regexp.test(password) !== true) {
      return { password: false }
    } else {
      return null
    }
  }

  canLoading() {
    if (!this.loginForm.valid) {
      this.loading = false
    } else {
      this.loading = true
    }
  }
}
