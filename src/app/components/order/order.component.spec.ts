import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { OrderComponent } from './order.component'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { OrderService } from 'src/app/core/_services/order.service'
import { HttpClient } from '@angular/common/http'
import { of, Observable } from 'rxjs'
import { Order } from 'src/app/core/_models'

describe('OrderComponent', () => {
  let component: OrderComponent
  let fixture: ComponentFixture<OrderComponent>

  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }
  let orderServiceSpy: { getOrders: jasmine.Spy; addOrder: jasmine.Spy }

  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    orderServiceSpy = jasmine.createSpyObj('OrderService', ['getOrders', 'addOrder'])

    TestBed.configureTestingModule({
      declarations: [OrderComponent],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, NgbRatingModule],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: OrderService, useValue: orderServiceSpy },
        { provide: HttpClient }
      ]
    }).compileComponents()
    fixture = TestBed.createComponent(OrderComponent)
    component = fixture.componentInstance
  }))

  it('should create', () => {
    orderServiceSpy.getOrders.and.returnValue(of())
    expect(component).toBeTruthy()
  })
})
