import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { environment } from 'src/environments/environment'
import { tap, catchError } from 'rxjs/operators'
import { Item } from '../_models/item'

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) {}

  addOrder(orderStatus: string, products: Item[]): Observable<any> {
    const token = localStorage.getItem('token')
    const headers = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': `${token}` })
    }
    console.log(token)
    console.log('addReview')
    console.log(`${environment.apiUrl}/api/order`, { headers: headers })
    const data = {
      orderStatus: orderStatus,
      products: products
    }
    console.log(data)
    return this.http.post(`${environment.apiUrl}/api/order`, data, headers).pipe(
      tap(response => console.log(response)),
      catchError(this.handleError<any>('add order'))
    )
  }

  getOrders(): Observable<any> {
    const token = localStorage.getItem('token')
    const headers = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': `${token}` })
    }
    return this.http.get<any>(`${environment.apiUrl}/api/order/me`, headers).pipe(
      tap(_ => console.log('fetched orders')),
      catchError(this.handleError<any>('getOrders', []))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
