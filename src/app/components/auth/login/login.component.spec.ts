import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { LoginComponent } from './login.component'
import { RouterLinkStubDirective } from 'src/app/app.component.spec'
import { Component } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { Http } from '@angular/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { of } from 'rxjs'
import { User } from '../../users/user.model'

describe('LoginComponent', () => {
  let component: LoginComponent
  let fixture: ComponentFixture<LoginComponent>

  let routerSpy: { navigateByUrl: jasmine.Spy }
  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }
  let authServiceSpy: { login: jasmine.Spy; userIsLoggedIn: jasmine.Spy; userFullName: jasmine.Spy }
  let userIsLoggedInSpy: { userIsLoggedIn: jasmine.Spy }

  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])
    authServiceSpy = jasmine.createSpyObj('authService', ['login', 'userIsLoggedIn'])
    userIsLoggedInSpy = jasmine.createSpyObj('authService', ['userIsLoggedIn'])

    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: Http }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(LoginComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('loginForm invalid when empty', async(() => {
    component.ngOnInit()
    component.loginForm.controls['email'].setValue('')
    component.loginForm.controls['password'].setValue('')
    expect(component.loginForm.valid).toBeFalsy()
  }))

  it('loginForm valid when not empty', async(() => {
    component.ngOnInit()
    component.loginForm.controls['email'].setValue('test@test.com')
    component.loginForm.controls['password'].setValue('Test123456789')
    expect(component.loginForm.valid).toBeTruthy()
  }))

  it('email field validity/empty', async(() => {
    component.ngOnInit()
    let email = component.loginForm.controls['email']
    expect(email.valid).toBeFalsy()
  }))

  it('password field validity/empty', async(() => {
    component.ngOnInit()
    let password = component.loginForm.controls['password']
    expect(password.valid).toBeFalsy()
  }))

  it('email field validity/errors working', async(() => {
    component.ngOnInit()
    let errors = {}
    let email = component.loginForm.controls['email']
    errors = email.errors || {}
    expect(errors['required']).toBeTruthy()
  }))

  it('password field validit/errors working', async(() => {
    component.ngOnInit()
    let errors = {}
    let password = component.loginForm.controls['password']
    errors = password.errors || {}
    expect(errors['required']).toBeTruthy()
  }))

  it('invalid emailadress', async(() => {
    component.ngOnInit()
    let errors = {}
    let email = component.loginForm.controls['email']
    email.setValue('testtest.com')
    errors = email.errors || {}
    expect(errors['pattern']).toBeTruthy()
  }))

  it('valid emailadress', async(() => {
    component.ngOnInit()
    let errors = {}
    let email = component.loginForm.controls['email']
    email.setValue('test@test.com')
    errors = email.errors || {}
    expect(errors['pattern']).toBeFalsy()
  }))

  it('invalid password', async(() => {
    component.ngOnInit()
    let errors = {}
    let password = component.loginForm.controls['password']
    password.setValue('1')
    errors = password.errors || {}
    expect(errors['pattern']).toBeTruthy()
  }))

  it('valid password', async(() => {
    component.ngOnInit()
    let errors = {}
    let password = component.loginForm.controls['password']
    password.setValue('A123456')
    errors = password.errors || {}
    expect(errors['pattern']).toBeFalsy()
  }))

  it('should display login screen when user has not already been logged in', async(() => {
    authServiceSpy.login.and.returnValue(of(false))
    authServiceSpy.userIsLoggedIn.and.returnValue(of(false))
    userIsLoggedInSpy.userIsLoggedIn.and.returnValue(of(false))

    component.ngOnInit()
    fixture.detectChanges()

    // The component subscribes to an asynchronous Observable in ngOnInit, therefore
    // we have to wait until that subscription returns -> .whenStable().
    fixture.whenStable().then(() => {
      fixture.detectChanges()
      expect(component).toBeTruthy()
    })
  }))

  it('should not display login screen when user has already been logged in', async(() => {
    authServiceSpy.userIsLoggedIn.and.returnValue(of(true))
    userIsLoggedInSpy.userIsLoggedIn.and.returnValue(of(true))
    component.ngOnInit()

    // The component subscribes to an asynchronous Observable in ngOnInit, therefore
    // we have to wait until that subscription returns -> .whenStable().

    fixture.whenStable().then(() => {
      fixture.detectChanges()
      expect(component).toBeTruthy()
      // Check your expectations here!
    })
  }))

  it('should create', async(() => {
    fixture.detectChanges()
    expect(component).toBeTruthy()
  }))
})
