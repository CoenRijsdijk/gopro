import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { User } from '../user.model'
import { UserService } from '../users.service'
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap'
import { Observable } from 'rxjs'
import { AuthenticationService } from 'src/app/core/_services'
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  isLoggedIn$: Observable<boolean>
  isAdmin$: Observable<boolean>
  isActive: boolean
  updateForm: FormGroup
  title: string
  // editMode switches between editing existing user or creating a new user.
  // Default is false (= create new user)
  editMode: boolean
  id: number
  user: User
  submitted = false
  dob

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService
  ) {}

  ngOnInit() {
    this.isLoggedIn$ = this.authService.userIsLoggedIn
    // Get my user information - get from server, based on JWT token
    this.userService.read().subscribe(user => (this.user = new User(user)))
    console.log(this.user)
    console.log(this.user._id)
    this.title = this.route.snapshot.data['title'] || 'Edit User'
    console.log(this.user)

    this.updateForm = new FormGroup({
      firstName: new FormControl(this.user.name.firstname, [Validators.required]),
      lastName: new FormControl(this.user.name.lastname, [Validators.required]),
      email: new FormControl(this.user.email, [Validators.required, this.validEmail.bind(this)]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
        this.validPassword.bind(this)
      ]),
      street: new FormControl(this.user.street, [Validators.required]),
      houseNumber: new FormControl(this.user.housenumber, [Validators.required]),
      postalCode: new FormControl(this.user.postalcode, [Validators.required]),
      city: new FormControl(this.user.city, [Validators.required]),
      country: new FormControl(this.user.country, [Validators.required])
    })
  }
  get f() {
    return this.updateForm.controls
  }

  onSubmit() {
    this.submitted = true
    console.log('onSubmit')

    if (this.updateForm.valid) {
      this.user.name.firstname = this.updateForm.value['firstName']
      this.user.name.lastname = this.updateForm.value['lastName']
      this.user.email = this.updateForm.value['email']
      this.user.password = this.updateForm.value['password']
      this.user.street = this.updateForm.value['street']
      this.user.housenumber = this.updateForm.value['houseNumber']
      this.user.postalcode = this.updateForm.value['postalCode']
      this.user.city = this.updateForm.value['city']
      this.user.country = this.updateForm.value['country']
      this.userService.updateUser(this.user)
    } else {
      console.error('reviewForm invalid')
    }
    // Save user via the service
    // Then navigate back to display view (= UserDetails).
    // The display view must then show the new or edited user.
    this.router.navigate(['..'], { relativeTo: this.route })
  }
  validEmail(control: FormControl): { [s: string]: boolean } {
    const email = control.value
    const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    if (regexp.test(email) !== true) {
      return { email: false }
    } else {
      return null
    }
  }

  validPassword(control: FormControl): { [s: string]: boolean } {
    const password = control.value
    const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
    const test = regexp.test(password)
    if (regexp.test(password) !== true) {
      return { password: false }
    } else {
      return null
    }
  }
}
