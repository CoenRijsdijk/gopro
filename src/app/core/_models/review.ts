import { User } from './user'

export class Review {
  id: number
  title: string
  description: string
  rating: number
  postingDate: Date
  user: User

  constructor(values: Object = {}) {
    Object.assign(this, values)
  }
}
