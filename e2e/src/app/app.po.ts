import { browser, by, element } from 'protractor'
import { CommonPageObject } from '../common.po'

export class AppPage extends CommonPageObject {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>
  }

  getTitleText() {
    return element(by.css('app-root .nav-link')).getText() as Promise<string>
  }
}
